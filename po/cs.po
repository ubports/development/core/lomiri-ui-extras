# Czech translation for ubuntu-ui-extras
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd.
# This file is distributed under the same license as the ubuntu-ui-extras package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-ui-extras\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-19 16:19+0000\n"
"PO-Revision-Date: 2023-08-04 18:04+0000\n"
"Last-Translator: Milan Korecky <milan.korecky@gmail.com>\n"
"Language-Team: Czech <https://hosted.weblate.org/projects/lomiri/lomiri-ui-"
"extras/cs/>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Weblate 5.0-dev\n"
"X-Launchpad-Export-Date: 2017-03-19 06:38+0000\n"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:106
msgid "Aborted"
msgstr "Přerušeno"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:108
msgid "Active"
msgstr "Aktivní"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:215
#: modules/Lomiri/Components/Extras/PhotoEditor/ExposureAdjuster.qml:72
msgid "Cancel"
msgstr "Zrušit"

#: modules/Lomiri/Components/Extras/Printers/backend/backend_pdf.cpp:44
msgid "Color"
msgstr "Barva"

#: modules/Lomiri/Components/Extras/Printers/models/printermodel.cpp:54
msgid "Create PDF"
msgstr "Vytvořit PDF"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:43
#: modules/Lomiri/Components/Extras/PhotoEditor/CropOverlay.qml:349
msgid "Crop"
msgstr "Oříznout"

#: modules/Lomiri/Components/Extras/PhotoEditor/ExposureAdjuster.qml:80
msgid "Done"
msgstr "Hotovo"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:235
msgid "Enhancing photo..."
msgstr "Vylepšování fotografie…"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:61
msgid "Exposure"
msgstr "Vystavení"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:104
msgid "Idle"
msgstr "Nečinná"

#: modules/Lomiri/Components/Extras/Printers/utils.h:61
msgid "Long Edge (Standard)"
msgstr "Dlouhá strana (standard)"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:115
msgid "No messages"
msgstr "Žádné zprávy"

#: modules/Lomiri/Components/Extras/Printers/backend/backend_pdf.cpp:47
msgid "Normal"
msgstr "Normální"

#: modules/Lomiri/Components/Extras/Printers/utils.h:64
msgid "One Sided"
msgstr "Jednostranná"

#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:121
msgid "Redo"
msgstr "Znovu"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:221
msgid "Revert Photo"
msgstr "Vrátit zpět fotku"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:205
#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:128
msgid "Revert to original"
msgstr "Vrátit do původního stavu"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:52
msgid "Rotate"
msgstr "Otočit"

#: modules/Lomiri/Components/Extras/Printers/utils.h:59
msgid "Short Edge (Flip)"
msgstr "Krátká strana (převrátit)"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:110
msgid "Stopped"
msgstr "Zastaveno"

#: modules/Lomiri/Components/Extras/Printers/printers/printers.cpp:395
msgid "Test page"
msgstr "Testovací stránka"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:206
msgid "This will undo all edits, including those from previous sessions."
msgstr "Toto odstraní všechny úpravy, včetně těch z předchozích sezení."

#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:114
msgid "Undo"
msgstr "Vrátit"
