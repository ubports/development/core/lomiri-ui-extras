# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the lomiri-ui-extras package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-ui-extras\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-19 16:19+0000\n"
"PO-Revision-Date: 2023-01-05 10:54+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: French (Switzerland) <https://hosted.weblate.org/projects/"
"lomiri/lomiri-ui-extras/fr_CH/>\n"
"Language: fr_CH\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:106
msgid "Aborted"
msgstr ""

#: modules/Lomiri/Components/Extras/Example/Printers.qml:108
msgid "Active"
msgstr ""

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:215
#: modules/Lomiri/Components/Extras/PhotoEditor/ExposureAdjuster.qml:72
#, fuzzy
msgid "Cancel"
msgstr "Cancel"

#: modules/Lomiri/Components/Extras/Printers/backend/backend_pdf.cpp:44
msgid "Color"
msgstr ""

#: modules/Lomiri/Components/Extras/Printers/models/printermodel.cpp:54
msgid "Create PDF"
msgstr ""

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:43
#: modules/Lomiri/Components/Extras/PhotoEditor/CropOverlay.qml:349
msgid "Crop"
msgstr ""

#: modules/Lomiri/Components/Extras/PhotoEditor/ExposureAdjuster.qml:80
msgid "Done"
msgstr ""

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:235
msgid "Enhancing photo..."
msgstr ""

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:61
msgid "Exposure"
msgstr ""

#: modules/Lomiri/Components/Extras/Example/Printers.qml:104
msgid "Idle"
msgstr ""

#: modules/Lomiri/Components/Extras/Printers/utils.h:61
msgid "Long Edge (Standard)"
msgstr ""

#: modules/Lomiri/Components/Extras/Example/Printers.qml:115
msgid "No messages"
msgstr ""

#: modules/Lomiri/Components/Extras/Printers/backend/backend_pdf.cpp:47
msgid "Normal"
msgstr ""

#: modules/Lomiri/Components/Extras/Printers/utils.h:64
msgid "One Sided"
msgstr ""

#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:121
msgid "Redo"
msgstr ""

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:221
msgid "Revert Photo"
msgstr ""

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:205
#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:128
msgid "Revert to original"
msgstr ""

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:52
msgid "Rotate"
msgstr ""

#: modules/Lomiri/Components/Extras/Printers/utils.h:59
msgid "Short Edge (Flip)"
msgstr ""

#: modules/Lomiri/Components/Extras/Example/Printers.qml:110
msgid "Stopped"
msgstr ""

#: modules/Lomiri/Components/Extras/Printers/printers/printers.cpp:395
msgid "Test page"
msgstr ""

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:206
msgid "This will undo all edits, including those from previous sessions."
msgstr ""

#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:114
msgid "Undo"
msgstr ""
