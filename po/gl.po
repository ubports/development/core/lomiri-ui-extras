# Galician translation for ubuntu-ui-extras
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd.
# This file is distributed under the same license as the ubuntu-ui-extras package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-ui-extras\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-19 16:19+0000\n"
"PO-Revision-Date: 2020-03-07 11:22+0000\n"
"Last-Translator: Neutrum N <neutro@tutanota.com>\n"
"Language-Team: Galician <https://translate.ubports.com/projects/ubports/"
"ubuntu-ui-extras/gl/>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.8\n"
"X-Launchpad-Export-Date: 2017-03-19 06:38+0000\n"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:106
msgid "Aborted"
msgstr "Abortado"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:108
msgid "Active"
msgstr "Activo"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:215
#: modules/Lomiri/Components/Extras/PhotoEditor/ExposureAdjuster.qml:72
msgid "Cancel"
msgstr "Cancelar"

#: modules/Lomiri/Components/Extras/Printers/backend/backend_pdf.cpp:44
msgid "Color"
msgstr "Cor"

#: modules/Lomiri/Components/Extras/Printers/models/printermodel.cpp:54
msgid "Create PDF"
msgstr "Crear PDF"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:43
#: modules/Lomiri/Components/Extras/PhotoEditor/CropOverlay.qml:349
msgid "Crop"
msgstr "Recortar"

#: modules/Lomiri/Components/Extras/PhotoEditor/ExposureAdjuster.qml:80
msgid "Done"
msgstr "Feito"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:235
msgid "Enhancing photo..."
msgstr "Mellorando a foto..."

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:61
msgid "Exposure"
msgstr ""

#: modules/Lomiri/Components/Extras/Example/Printers.qml:104
msgid "Idle"
msgstr "Inactivo"

#: modules/Lomiri/Components/Extras/Printers/utils.h:61
msgid "Long Edge (Standard)"
msgstr "Beira longa (estándar)"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:115
msgid "No messages"
msgstr "Sen mensaxes"

#: modules/Lomiri/Components/Extras/Printers/backend/backend_pdf.cpp:47
msgid "Normal"
msgstr "Normal"

#: modules/Lomiri/Components/Extras/Printers/utils.h:64
msgid "One Sided"
msgstr "Unha cara"

#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:121
msgid "Redo"
msgstr "Refacer"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:221
msgid "Revert Photo"
msgstr "Reverter foto"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:205
#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:128
msgid "Revert to original"
msgstr "Reverter ao orixinal"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:52
msgid "Rotate"
msgstr "Rotar"

#: modules/Lomiri/Components/Extras/Printers/utils.h:59
msgid "Short Edge (Flip)"
msgstr "Beira curta (voltear)"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:110
msgid "Stopped"
msgstr "Parado"

#: modules/Lomiri/Components/Extras/Printers/printers/printers.cpp:395
msgid "Test page"
msgstr "Páxina de proba"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:206
msgid "This will undo all edits, including those from previous sessions."
msgstr ""
"Isto desfará todas as modificacións, incluídas as de sesións anteriores."

#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:114
msgid "Undo"
msgstr "Desfacer"
