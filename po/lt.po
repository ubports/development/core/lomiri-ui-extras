# Lithuanian translation for ubuntu-ui-extras
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd.
# This file is distributed under the same license as the ubuntu-ui-extras package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-ui-extras\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-19 16:19+0000\n"
"PO-Revision-Date: 2019-09-25 20:51+0000\n"
"Last-Translator: Moo <moose@mail.ru>\n"
"Language-Team: Lithuanian <https://translate.ubports.com/projects/ubports/"
"ubuntu-ui-extras/lt/>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n % 10 == 1 && (n % 100 < 11 || n % 100 > "
"19)) ? 0 : ((n % 10 >= 2 && n % 10 <= 9 && (n % 100 < 11 || n % 100 > 19)) ? "
"1 : 2);\n"
"X-Generator: Weblate 3.8\n"
"X-Launchpad-Export-Date: 2017-03-19 06:38+0000\n"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:106
msgid "Aborted"
msgstr "Nutraukta"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:108
msgid "Active"
msgstr "Aktyvus"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:215
#: modules/Lomiri/Components/Extras/PhotoEditor/ExposureAdjuster.qml:72
msgid "Cancel"
msgstr "Atsisakyti"

#: modules/Lomiri/Components/Extras/Printers/backend/backend_pdf.cpp:44
msgid "Color"
msgstr "Spalva"

#: modules/Lomiri/Components/Extras/Printers/models/printermodel.cpp:54
msgid "Create PDF"
msgstr "Sukurti PDF"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:43
#: modules/Lomiri/Components/Extras/PhotoEditor/CropOverlay.qml:349
msgid "Crop"
msgstr "Apkirpti"

#: modules/Lomiri/Components/Extras/PhotoEditor/ExposureAdjuster.qml:80
msgid "Done"
msgstr "Atlikta"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:235
msgid "Enhancing photo..."
msgstr "Tobulinama nuotrauka..."

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:61
msgid "Exposure"
msgstr ""

#: modules/Lomiri/Components/Extras/Example/Printers.qml:104
msgid "Idle"
msgstr "Neveiklus"

#: modules/Lomiri/Components/Extras/Printers/utils.h:61
msgid "Long Edge (Standard)"
msgstr ""

#: modules/Lomiri/Components/Extras/Example/Printers.qml:115
msgid "No messages"
msgstr ""

#: modules/Lomiri/Components/Extras/Printers/backend/backend_pdf.cpp:47
msgid "Normal"
msgstr ""

#: modules/Lomiri/Components/Extras/Printers/utils.h:64
msgid "One Sided"
msgstr ""

#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:121
msgid "Redo"
msgstr "Grąžinti"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:221
msgid "Revert Photo"
msgstr "Sugrąžinti nuotrauką"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:205
#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:128
msgid "Revert to original"
msgstr "Sugrąžinti į pradinę"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:52
msgid "Rotate"
msgstr "Pasukti"

#: modules/Lomiri/Components/Extras/Printers/utils.h:59
msgid "Short Edge (Flip)"
msgstr ""

#: modules/Lomiri/Components/Extras/Example/Printers.qml:110
msgid "Stopped"
msgstr "Sustabdytas"

#: modules/Lomiri/Components/Extras/Printers/printers/printers.cpp:395
msgid "Test page"
msgstr ""

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:206
msgid "This will undo all edits, including those from previous sessions."
msgstr ""
"Tai atšauks visus redagavimus, įskaitant tuos, kurie buvo atlikti "
"ankstesniuose seansuose."

#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:114
msgid "Undo"
msgstr "Atšaukti"
