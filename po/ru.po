# Russian translation for ubuntu-ui-extras
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd.
# This file is distributed under the same license as the ubuntu-ui-extras package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-ui-extras\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-19 16:19+0000\n"
"PO-Revision-Date: 2023-01-31 12:43+0000\n"
"Last-Translator: Sergii Horichenko <m@sgg.im>\n"
"Language-Team: Russian <https://hosted.weblate.org/projects/lomiri/lomiri-ui-"
"extras/ru/>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.16-dev\n"
"X-Launchpad-Export-Date: 2017-03-19 06:38+0000\n"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:106
msgid "Aborted"
msgstr "Отменено"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:108
msgid "Active"
msgstr "Активный"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:215
#: modules/Lomiri/Components/Extras/PhotoEditor/ExposureAdjuster.qml:72
msgid "Cancel"
msgstr "Отмена"

#: modules/Lomiri/Components/Extras/Printers/backend/backend_pdf.cpp:44
msgid "Color"
msgstr "Цвет"

#: modules/Lomiri/Components/Extras/Printers/models/printermodel.cpp:54
msgid "Create PDF"
msgstr "Создать PDF"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:43
#: modules/Lomiri/Components/Extras/PhotoEditor/CropOverlay.qml:349
msgid "Crop"
msgstr "Кадрировать"

#: modules/Lomiri/Components/Extras/PhotoEditor/ExposureAdjuster.qml:80
msgid "Done"
msgstr "Сделано"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:235
msgid "Enhancing photo..."
msgstr "Улучшение снимка..."

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:61
msgid "Exposure"
msgstr "Экспозиция"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:104
msgid "Idle"
msgstr "Простаивает"

#: modules/Lomiri/Components/Extras/Printers/utils.h:61
msgid "Long Edge (Standard)"
msgstr "Длинный край (стандартный)"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:115
msgid "No messages"
msgstr "Нет сообщений"

#: modules/Lomiri/Components/Extras/Printers/backend/backend_pdf.cpp:47
msgid "Normal"
msgstr "Нормальный"

#: modules/Lomiri/Components/Extras/Printers/utils.h:64
msgid "One Sided"
msgstr "Односторонний"

#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:121
msgid "Redo"
msgstr "Повторить"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:221
msgid "Revert Photo"
msgstr "Восстановить снимок"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:205
#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:128
msgid "Revert to original"
msgstr "Вернуться к оригиналу"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:52
msgid "Rotate"
msgstr "Вращать"

#: modules/Lomiri/Components/Extras/Printers/utils.h:59
msgid "Short Edge (Flip)"
msgstr "Короткий край (флип)"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:110
msgid "Stopped"
msgstr "Остановлен"

#: modules/Lomiri/Components/Extras/Printers/printers/printers.cpp:395
msgid "Test page"
msgstr "Пробная страница"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:206
msgid "This will undo all edits, including those from previous sessions."
msgstr "Это отменит все изменения, включая изменения из предыдущих сеансов."

#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:114
msgid "Undo"
msgstr "Отменить"
