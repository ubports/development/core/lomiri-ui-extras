include_directories(
    ${CMAKE_CURRENT_BINARY_DIR}
    ${CMAKE_SOURCE_DIR}/modules/Lomiri/Components/Extras/plugin/example/
    ${CMAKE_SOURCE_DIR}/modules/Lomiri/Components/Extras/plugin/photoeditor/
    )

qt_add_resources(sampledata_rc_srcs sampledata.qrc)
add_custom_target(sampledata_rc_target DEPENDS ${sampledata_rc_srcs})

set(plugin-dir ../../modules/Lomiri/Components/Extras/plugin)
macro(generate_tests)
    foreach(test ${ARGN})
        add_executable(${test}
            ${test}.cpp
            ${plugin-dir}/photoeditor/photo-image-provider.cpp
            ${sampledata_rc_srcs}
            )
        target_link_libraries(${test}
            ${TPL_QT5_LIBRARIES}
            lomiri-ui-extras-plugin
            Qt::Core
            Qt::Qml
            Qt::Quick
            Qt::Test
            )
        add_dependencies(${test} sampledata_rc_target)
        add_test(${test}
            ${CMAKE_CURRENT_BINARY_DIR}/${test}
            -xunitxml -o ${test}.xml
            )
        set_tests_properties(${test} PROPERTIES
            ENVIRONMENT "QT_QPA_PLATFORM=minimal"
            )
    endforeach(test)
endmacro(generate_tests)

generate_tests(
    tst_ExampleModelTests
    tst_PhotoEditorPhoto
    tst_PhotoEditorPhotoImageProvider
    )

add_subdirectory(Printers)
